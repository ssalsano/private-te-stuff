#!/usr/bin/python

##############################################################################################
# Copyright (C) 2014 Pier Luigi Ventre - (Consortium GARR and University of Rome "Tor Vergata")
# Copyright (C) 2014 Giuseppe Siracusano, Stefano Salsano - (CNIT and University of Rome "Tor Vergata")
# www.garr.it - www.uniroma2.it/netgroup - www.cnit.it
#
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# te traffic engineering
#
# @author Pier Luigi Ventre <pl.ventre@gmail.com>
# @author Giuseppe Siracusano <a_siracusano@tin.it>
# @author Stefano Salsano <stefano.salsano@uniroma2.it>
#
#

import os
import json
import argparse
import sys

from random import randrange
import networkx as nx

from utility import *


cfg = {}
key_dst = "dst"
key_src = "src"
key_dpid = "dpid"
key_name = "name"

key_vss = "vss"
key_cer_id = "cer_id"
key_lhs_id = "lhs_id"
key_lhs_gre_ip = "lhs_gre_ip"
key_lhs_gre_mac = "lhs_gre_mac"
key_lhs_intf = "lhs_intf"
key_lhs_label = "lhs_label"
key_rhs_id = "rhs_id"
key_rhs_gre_ip = "rhs_gre_ip"
key_rhs_gre_mac = "rhs_gre_mac"
key_rhs_intf = "rhs_intf"
key_rhs_label = "rhs_label"
key_cid = "cid"
key_pws = "pws"
key_id = "id"
key_customers_vtep = "customers_vtep"
BIGK = 100			#it is used by the set_weights_on_available_capa
MAXWEIGHT = 10000000	#it is used by the multidigraph2digraph function

# a flow catalogue entry is { key : (src, dst, {'out_size': xx , 'out_allocated':boolean, 'in_size': yy, 'in_allocated':boolean} ) }
# a flow catalogue entry is { key : (src, dst, {'out': {'size': xx , 'allocated':boolean}, 'in': {'size': yy, 'allocated':boolean}} ) }
# currently keys are integer, but this can be changed
#
flow_catalogue = {}



#check if any adjacency has more than one edge
#note that if a property is added at the adjacency level, it is seen as an additional edge
def check_if_multilink(nx_multidigraph):
	for n,adjacency_dict in nx_multidigraph.adjacency_iter():
		#print(adjacency_dict)
		for adjacency in adjacency_dict:
			if (len(adjacency_dict[adjacency])) > 1:
				return True
	return False

# set weight on each edge as BIGK/(edge_capacity-allocated)
# if capacity is not defined the weight is set to 1 
# if allocated is not defined the weight is set to BIGK/edge_capacity
def set_weights_on_available_capa(nx_multidigraph):
	for edge in nx_multidigraph.edges_iter(data=True):
		#print(edge)
		#edge[2]['weight']=BIGK/(edge[2]['capacity']-edge[2]['allocated'])
		edge[2]['weight']=BIGK/(edge[2].get('capacity',BIGK)-edge[2].get('allocated',0))
	return	

# set weight on each edge as BIGK/edge_capacity
# if capacity is not defined the weight is set to 1 
def set_weights_on_capacity(nx_multidigraph):
	for edge in nx_multidigraph.edges_iter(data=True):
		#print(edge)
		edge[2]['weight']=BIGK/(edge[2].get(['capacity'],BIGK))
	return	
	
	
#returns a Digraph from a MultiDiGraph, selecting the edge with minimal weight among the edges that
#have the 'weight' properties set
#if no 'weight' properties is set, it selects the first edge
#it can be safely used if the MultiDiGraph has no real multilink
#it uses the MAXWEIGHT constant
def multidigraph2digraph(nx_multidigraph):
	GG=nx.DiGraph()
	for n,nbrs in nx_multidigraph.adjacency_iter():
		#print (n)
		#print (nbrs)
		#print (nbrs.items())
		for nbr,edict in nbrs.items():
		#for nbr,key_edict in nbrs:
			#print('edict>>>'+str(edict))
			properties_dict = edict[0]
			#minimum = edict[0]['weight']
			minimum = edict[0].get('weight', MAXWEIGHT)
			#print ('minimum: '+str(minimum))
			for d in edict.values():
				#print ('values: '+str(d))
				if d.get('weight', MAXWEIGHT)<minimum:
					minimum=d['weight']
					properties_dict=d
				
			#GG.add_edge(n,nbr, weight = minvalue)
			GG.add_edge(n,nbr, properties_dict)
	return (GG)
  
# prunes a graph or a digraph by removing all edges with capacity < size
# does not work on multigraph
# edges with no 'capacity' defined are removed by default
# unless tolerance=True which accepts all edges with no 'capacity' defined
def prune_graph_by_capacity(nx_graph, size, tolerance=False):
	for edge in nx_graph.edges_iter(data=True):
		if 'capacity' in edge[2]:
			#print (edge[2]['capacity'])
			if (edge[2]['capacity']>=size):
				continue
		else:
			if tolerance:
				continue
		#print ('no capacity for edge:'+str(edge))
		nx_graph.remove_edge(edge[0],edge[1])
  
# prunes a graph or a digraph by removing all edges with (capacity-allocated) < size
# does not work on multigraph
# edges with no 'capacity' defined are removed by default, 
# if 'allocated' is not defined it is assumed to be 0
# unless tolerance=True which accepts all edges with no 'capacity' defined
def prune_graph_by_available_capacity(nx_graph, size, tolerance=False):
	for edge in nx_graph.edges_iter(data=True):
		if 'capacity' in edge[2]:
			#print (edge[2]['capacity'])
			if (edge[2]['capacity']-edge[2].get('allocated',0)>=size):
				continue
		else:
			if tolerance:
				continue
		#print ('no capacity for edge:'+str(edge))
		nx_graph.remove_edge(edge[0],edge[1])

# prunes a multigraph or a multidigraph by removing all edges with capacity < size  
#def
		
# prunes a multigraph or a multidigraph by removing all edges with (capacity-allocated) < size  
#def

#returns a multidigraph representing the traffic flows from a flow_catalogue
#the traffic flows here are unidirectional
def multidigraph_from_flow_catalogue (fc_dict):
	nx_flows = nx.MultiDiGraph()
	for flow_id, (src, dst, flow_dict) in fc_dict.iteritems():
		#print(str(flow_id)+' : '+str(flow_dict))
		if 'out' in flow_dict:
			if 'size' in flow_dict['out']:
				nx_flows.add_edge(src, dst, flow_id, {'size':flow_dict['out']['size']})
		if 'in' in flow_dict:
			if 'size' in flow_dict['in']:
				nx_flows.add_edge(dst, src, flow_id, {'size':flow_dict['in']['size']})
	#print('nx_flows edges inside:'+str(nx_flows.edges()))
	return nx_flows

#returns an edge of the multidigraph representing the traffic flows from (flow_id, src, dst)
def nx_edge_from_flow (nx_multidigraph, flow_id, src, dst):
	return nx_multidigraph[src][dst][flow_id]
	
# allocates the flow in the network
def allocate_flow (nx_digraph, path, size):
	for i in range (0 , len(path)-1):
		#print (path[i])
		nx_digraph[path[i]][path[i+1]]['allocated']=nx_digraph[path[i]][path[i+1]]['allocated']+size
		#print (nx_digraph[path[i]][path[i+1]]['allocated'])
		
#stores the path in the multidigraph representing the traffic flows
def store_path (nx_multidigraph, src,dst,flow_id, path):
	nx_multidigraph[src][dst][flow_id]['path']=path

# set / unset the 'allocated' flag in the flow catalogue dict
# direction can be 'in' / 'out' / 'all'
def set_allocated (flow_catalogue, flow_id, direction='all', allocated=True):
	if direction == 'out':
		flow_catalogue[flow_id][2]['out']['allocated']=allocated
	if direction == 'in':
		flow_catalogue[flow_id][2]['in']['allocated']=allocated
	if direction == 'all':
		if 'out' in flow_catalogue[flow_id]:
			set_allocated (flow_catalogue, flow_id, 'out', allocated)
		if 'in' in flow_catalogue[flow_id]:
			set_allocated (flow_catalogue, flow_id, 'in', allocated)
	
	
def o_selection(args):

	#load_cfg()
	#controllerRestIp = args.controllerRestIp

	#command = "curl -s http://%s/v1.0/topology/links | python -mjson.tool" % (controllerRestIp)
	#result = os.popen(command).read()
	#topology = json.loads(result)

	nx_topology = nx.MultiDiGraph()
	nx_topology_sp = nx.Graph()
	nx_topology_sp_exploded = nx.Graph()

	nx_topology.add_nodes_from([1,2,3,4,5,6,7])
	
	#non-multilink graph
	nx_topology.add_edges_from([(1,2),(2,1),(2,3),(3,2),(2,4),(4,2),(2,5),(5,2),(3,4),(4,3),(3,5),(5,3),(4,6),(6,4),(5,6),(6,5),(6,7),(7,6)],{'capacity':20,'allocated':0})
	
	#a multilink graph
	#nx_topology.add_edges_from([(1,2),(2,1),(2,3),(2,3),(2,3),(3,2),(2,4),(4,2),(2,5),(5,2),(3,4),(4,3),(3,5),(5,3),(4,6),(6,4),(5,6),(6,5),(6,7),(7,6)],{'capacity':20,'allocated':0})
	
	#print(nx_topology.nodes())
	#print(nx_topology.edges())
	#print(nx_topology.edge[1][2][0]['capacity'])
	#print(nx_topology[1][2])
	
	set_weights_on_available_capa(nx_topology)

	nx_topology.edge[2][3][0]['capacity']=50	#non-multilink case
	nx_topology.edge[2][3][0]['weight']=3	#non-multilink case

	
	#nx_topology.edge[2][3][2]['capacity']=50	#multilink case
	#nx_topology.edge[2][3][2]['weight']=3	#multilink case
	

	print('nx_topology edges:')
	print(list(nx_topology.edges_iter(data=True)))

	
	if check_if_multilink(nx_topology):
		print ('\nsorry,nx_topology is multilink, this is not yet supported :-(')
		sys.exit()

	#the multidigraph is turned into a digraph 
	nx_digraph = multidigraph2digraph(nx_topology)
	print('nx_digraph edges (nx_topology reduced to a digraph)')
	print(list(nx_digraph.edges_iter(data=True)))

		
	
	# creation of traffic flows from a flow catalogue
	flow_catalogue[0]=(1,7,{'out':{'size':10},'in': {'size':8}})
	flow_catalogue[1]=(1,7,{'out':{'size':200},'in': {'size':8}})
	flow_catalogue[2]=(1,7,{'out':{'size':12}})
	nx_flows = multidigraph_from_flow_catalogue(flow_catalogue)

	# (old : manual creatio of traffic flows into the nx_flows graph)
	#nx_flows = nx.MultiDiGraph()
	#nx_flows.add_nodes_from([1,7])
	#nx_flows.add_edges_from([(1,7,{'size':2}),(1,7,{'size':3}),(1,7,{'size':3}),(7,1,{'size':3})])
	
	print('nx_flows edges:')
	print(list(nx_flows.edges_iter(data=True)))
	
	#test pruning function
	#prune_graph_by_capacity(work_nx_digraph,10)
	#print('nx_digraph edges after pruning')
	#print(list(nx_digraph.edges_iter(data=True)))
	
	print('\n------------------flow catalogue:')
	print(flow_catalogue)
	
	print('\n')	
	for flow_id, (src, dst, flow_dict) in flow_catalogue.iteritems():
		work_nx_digraph=nx_digraph.copy()
		if 'out' in flow_dict and 'size' in flow_dict['out']:
			prune_graph_by_available_capacity(work_nx_digraph,flow_dict['out']['size'])
			print('dijkstra : '+str(src)+' >> '+str(dst)+', size: '+str(flow_dict['out']['size']))
			try:
				path = nx.dijkstra_path(work_nx_digraph,src,dst)
				allocate_flow (nx_digraph, path, flow_dict['out']['size'])
				store_path (nx_flows, src, dst, flow_id, path)
				set_allocated (flow_catalogue, flow_id, direction='out', allocated=True)
			except nx.NetworkXNoPath:
				path = []
			print(path)
				
		work_nx_digraph=nx_digraph.copy()
		if 'in' in flow_dict and 'size' in flow_dict['in']:
			prune_graph_by_available_capacity(work_nx_digraph,flow_dict['in']['size'])
			print('dijkstra : '+str(dst)+' >> '+str(src)+', size: '+str(flow_dict['in']['size']))
			try:
				path = nx.dijkstra_path(work_nx_digraph,dst,src)
				allocate_flow (nx_digraph, path, flow_dict['in']['size'])
				store_path (nx_flows, dst, src, flow_id, path)
				set_allocated (flow_catalogue, flow_id, direction='in', allocated=True)
			except nx.NetworkXNoPath:
				path = []
			print(path)
	
	
	
	#nx_multidigraph[src,dst,flow_id]
	
	
	print('\nnx_digraph edges (nx_topology reduced to a digraph)')
	print(list(nx_digraph.edges_iter(data=True)))
	
	print('\nnx_flows edges:')
	print(list(nx_flows.edges_iter(data=True)))
	
	print('\n------------------flow catalogue:')
	print(flow_catalogue)

	
	sys.exit()
	
	out_cfg = {}
	vss = []
	i = 0

	cid_to_vtep_allocator = {}

	for vs in cfg[key_vss]:

		cer_to_data = {}
		steiner_nodes = []


		i = i + 1 
		
		# First step (Building the topology considering multilink)
		nx_topology.clear()
		for link in topology:
			src =link[key_src][key_dpid]
			dst =link[key_dst][key_dpid]
			nx_topology.add_edge(src, dst, src_port=link[key_src][key_name], dst_port=link[key_src][key_name])

		j = 0
		# Second step (Adding CERs=Steiner Nodes))
		for pw in vs[key_pws]:
				j = j + 1
				cer = pw[key_cer_id].replace(":","")
				oshi = pw[key_lhs_id].replace(":","")
				nx_topology.add_edge(cer, oshi,	src_port="cer-eth%s" %(j), dst_port="peo-eth%s" %(j))
				nx_topology.add_edge(oshi, cer,	src_port="peo-eth%s" %(j), dst_port="cer-eth%s" %(j))
				cer_to_data[cer]=pw
				steiner_nodes.append(cer)
	
		# Second Step (Building the topology of the shortest paths)
		j = 0
		sps = []
		for j in range(0,len(steiner_nodes)-1):
			for k in range(j + 1,len(steiner_nodes)):
				sp = nx.shortest_path(nx_topology, steiner_nodes[j], steiner_nodes[k])
				links = []
				sp_map = {}
				for z in range(0, len(sp)-1):
					link = nx_topology[sp[z]][sp[z+1]]
					index = randrange(len(link))
					links.append((sp[z], sp[z+1], link[index]['src_port'], link[index]['dst_port']))
					#links.append((sp[z+1], sp[z], link[index]['dst_port'], link[index]['src_port']))
					
				sp_map['lhs'] = steiner_nodes[j]
				sp_map['rhs'] = steiner_nodes[k]
				sp_map['path'] = links
				sp_map['cost'] = len(links)/2
				sps.append(sp_map)
		
		# Third Step (Building the Spannig tree of the shortest paths topology)
		nx_topology_sp.clear()
		for sp in sps:
			nx_topology_sp.add_edge(sp['lhs'], sp['rhs'], weight=int(sp['cost']))

		T=nx.minimum_spanning_tree(nx_topology_sp)
	
		# Fourth step (Exploding the previous spanning tree)
		nx_topology_sp_exploded.clear()
		for edge in T.edges(data=False):
			for z in range(0, len(sps)):
				if (sps[z]['lhs'] == edge[0] and sps[z]['rhs'] == edge[1]) or (sps[z]['lhs'] == edge[1] and sps[z]['rhs'] == edge[0]):
					for link in sps[z]['path']:
						nx_topology_sp_exploded.add_edge(link[0],link[1], src_port=link[2], dst_port=link[3])

		# Fifth step (Building the spanning tree of the previous topology)
		T2=nx.minimum_spanning_tree(nx_topology_sp_exploded)

		# Sixth step (Eliminating non-steiner leaf)
		j = 0
		end = len(T2.nodes(data=False))
		while j < end:
			node = T2.nodes(data=False)[j]
			if T2.degree(node) == 1 and node not in steiner_nodes:
				T2.remove_node(node)
				j = 0
				end = len(T2.nodes(data=False))
				continue
			j = j + 1 

		# Seventh step (Optimization)
		j = 0
		end = len(T2.nodes(data=False))
		while j < end:
			node = T2.nodes(data=False)[j]
			if T2.degree(node) == 2:
				first_link = None
				second_link = None
				first_index = 0
				second_index = 0
				for	edge in T2.edges(data=False):
					if node in edge:					
						if not first_link:
							first_link = edge
							if node is edge[0]:
								first_index = 1
							if node is edge[1]:
								first_index = 0
						elif not second_link:
							second_link = edge
							if node is edge[0]:
								second_index = 1
							if node is edge[1]:
								second_index = 0
						else:
							print "ERROR IMPOSSIBLE DEGREE 3"
							exit(-1)	
				T2.add_edge(first_link[first_index], second_link[second_index])
				T2.remove_node(node)
				j = 0
				end = len(T2.nodes(data=False))
				continue
			j = j + 1

		cid = str(vs[key_cid])
		allocator = cid_to_vtep_allocator.get(cid, None)
		if not allocator:
			used = cfg[key_customers_vtep][cid]
			allocator = VTEPAllocator(used)	
			cid_to_vtep_allocator[cid] = allocator

		# Generating temp.cfg
		out_vs = {}
		out_pws = []	

		for	edge in T2.edges(data=False):
			out_pw = {}
			if edge[0] in steiner_nodes:
				pw = cer_to_data[edge[0]]
				
				out_pw[key_cer_id] = pw[key_cer_id]
			
				out_pw[key_lhs_gre_ip] = pw[key_lhs_gre_ip]
				out_pw[key_lhs_gre_mac] = pw[key_lhs_gre_mac]
				out_pw[key_lhs_id] = pw[key_lhs_id]
				out_pw[key_lhs_label] = pw[key_lhs_label]
				out_pw[key_lhs_intf] = pw[key_lhs_intf]

				out_pw[key_rhs_gre_ip] = pw[key_rhs_gre_ip]
				out_pw[key_rhs_gre_mac] = pw[key_rhs_gre_mac]
				out_pw[key_rhs_id] = ":".join(s.encode('hex') for s in edge[1].decode('hex'))
				out_pw[key_rhs_label] = pw[key_rhs_label]
				out_pw[key_rhs_intf] = pw[key_rhs_intf]

			elif edge[1] in steiner_nodes:
				pw = cer_to_data[edge[1]]
				
				out_pw[key_cer_id] = pw[key_cer_id]
			
				out_pw[key_lhs_gre_ip] = pw[key_lhs_gre_ip]
				out_pw[key_lhs_gre_mac] = pw[key_lhs_gre_mac]
				out_pw[key_lhs_id] = pw[key_lhs_id]
				out_pw[key_lhs_label] = pw[key_lhs_label]
				out_pw[key_lhs_intf] = pw[key_lhs_intf]

				out_pw[key_rhs_gre_ip] = pw[key_rhs_gre_ip]
				out_pw[key_rhs_gre_mac] = pw[key_rhs_gre_mac]
				out_pw[key_rhs_id] = ":".join(s.encode('hex') for s in edge[0].decode('hex'))
				out_pw[key_rhs_label] = pw[key_rhs_label]
				out_pw[key_rhs_intf] = pw[key_rhs_intf]
	
			elif edge[0] not in steiner_nodes and edge[1] not in steiner_nodes:
				
				out_pw[key_cer_id] = None
				
				vtep = allocator.next_vtep()
			
				out_pw[key_lhs_gre_ip] = vtep.IP
				out_pw[key_lhs_gre_mac] = ":".join(s.encode('hex') for s in vtep.MAC.decode('hex'))
				out_pw[key_lhs_id] = ":".join(s.encode('hex') for s in edge[0].decode('hex'))
				out_pw[key_lhs_label] = '0'
				#TODO modificare per ofelia (simulare nomi)
				out_pw[key_lhs_intf] = None

				vtep = allocator.next_vtep()

				out_pw[key_rhs_gre_ip] = vtep.IP
				out_pw[key_rhs_gre_mac] = ":".join(s.encode('hex') for s in vtep.MAC.decode('hex'))
				out_pw[key_rhs_id] = ":".join(s.encode('hex') for s in edge[1].decode('hex'))
				out_pw[key_rhs_label] = '0'
				#TODO modificare per ofelia (simulare nomi)
				out_pw[key_rhs_intf] = None

			else:
				print "ERROR IMPOSSIBLE"
				exit(-1)
			out_pws.append(out_pw)

		out_vs[key_cid] = vs[key_cid]
		out_vs[key_pws] = out_pws
		out_vs[key_id] = i
		vss.append(out_vs)


	out_cfg[key_vss] = vss
	outcfg_file = open('../temp.cfg','w')
	outcfg_file.write(json.dumps(out_cfg, sort_keys=True, indent=4))
	outcfg_file.close()

def u_selection(args):

	load_cfg()
	controllerRestIp = args.controllerRestIp

	command = "curl -s http://%s/v1.0/topology/links | python -mjson.tool" % (controllerRestIp)
	result = os.popen(command).read()
	topology = json.loads(result)
	
	#topology is the json result of /topology/links REST API

	command = "curl -s http://%s/v1.0/topology/switches | python -mjson.tool" % (controllerRestIp)
	result = os.popen(command).read()
	oshies = json.loads(result)	


	out_cfg = {}
	vss = []

	i = 0
	for vs in cfg[key_vss]:

		i = i + 1
	
		out_vs = {}
		out_pws = []

		oshs = []
		endoshs = []

		for pw in vs[key_pws]:
			oshi = pw[key_lhs_id].replace(":","")
			if oshi not in endoshs:
				endoshs.append(oshi)

		for link in topology:
			if link[key_dst][key_dpid] not in oshs and link[key_dst][key_dpid] not in endoshs:
				oshs.append(link[key_dst][key_dpid])
			if link[key_src][key_dpid] not in oshs and link[key_src][key_dpid] not in endoshs:
				oshs.append(link[key_src][key_dpid])

		index = randrange(len(oshs))
		print "VS(%s) - Selection %s" %(i, oshs[index])


		for pw in vs[key_pws]:

			out_pw = {}
			out_pw[key_cer_id] = pw[key_cer_id]
			
			out_pw[key_lhs_gre_ip] = pw[key_lhs_gre_ip]
			out_pw[key_lhs_gre_mac] = pw[key_lhs_gre_mac]
			out_pw[key_lhs_id] = pw[key_lhs_id]
			out_pw[key_lhs_label] = pw[key_lhs_label]
			out_pw[key_lhs_intf] = pw[key_lhs_intf]

			out_pw[key_rhs_gre_ip] = pw[key_rhs_gre_ip]
			out_pw[key_rhs_gre_mac] = pw[key_rhs_gre_mac]
			out_pw[key_rhs_id] = ":".join(s.encode('hex') for s in oshs[index].decode('hex'))
			out_pw[key_rhs_label] = pw[key_rhs_label]
			out_pw[key_rhs_intf] = pw[key_rhs_intf]

			out_pws.append(out_pw)

		out_vs[key_cid] = vs[key_cid]
		out_vs[key_pws] = out_pws
		out_vs[key_id] = i

		
		vss.append(out_vs)


	out_cfg[key_vss] = vss
	outcfg_file = open('../temp.cfg','w')
	outcfg_file.write(json.dumps(out_cfg, sort_keys=True, indent=4))
	outcfg_file.close()
		
		
def parse_cmd_line():
	parser = argparse.ArgumentParser(description='Virtual Switch Selector')
	#parser.add_argument('--controller', dest='controllerRestIp', action='store', default='localhost:8080', help='controller IP:RESTport, e.g., localhost:8080 or A.B.C.D:8080')
	parser.add_argument('-o', dest='selection', action='store_const', const='optimized', default='optimized', help='selection: optimized')
	parser.add_argument('-u', dest='selection', action='store_const', const='unoptimized', default='optimized', help='selection: unoptimized')
	args = parser.parse_args()
	if len(sys.argv)==1:
		parser.print_help()
		sys.exit(1)
	return args

if __name__ == '__main__':
	args = parse_cmd_line()
	if args.selection == "optimized":
		o_selection(args)
	elif args.selection == "unoptimized":
		u_selection(args)


	
